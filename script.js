function pickUpValidation(bodyObj) {
    if ($("#pickUpAddressCheck").is(':checked')) {
        var pickUpVal = $('#pick_up_address').val();
        if (IsTextEmpty(pickUpVal)) {
            alert('Please provide Pick up address');
            return false;
        } else {
            bodyObj.pick_up_address = pickUpVal
        }

    } else {
        var pickAirpot = $('#pick_up_airport').val();
        var pickFligt = $('#pick_up_flight').val();
        var pickUpComing = $('#pick_up_coming').val();


        if (IsTextEmpty(pickAirpot)) {
            alert('Please Select Pick up Airport')
            return false;
        }
        if (IsTextEmpty(pickFligt)) {
            alert('Please Enter flight Number in Pick Up Section')
            return false;
        }

        if (IsTextEmpty(pickUpComing)) {
            alert('Please Enter Coming from in Pick Up Section')
            return false;
        }

        bodyObj.pick_from_airport = pickAirpot;
        bodyObj.pick_flight_number = pickFligt;
        bodyObj.pick_coming_from = pickUpComing;
        if ($('#airport_park_charge_pick_no').is(':checked'))
            bodyObj.pick_up_airpot_parking = 'no';

        if ($('#airport_park_charge_pick_yes').is(':checked'))
            bodyObj.pick_up_airpot_parking = 'yes';
    }
}

function dropUpValidation(bodyObj) {
    if ($("#dropUpAddressCheck").is(':checked')) {
        var dropOffVal = $('#drop_up_address').val();
        if (IsTextEmpty(dropOffVal)) {
            alert('Please provide Drop off address');
            return false;
        } else {
            bodyObj.drop_off_address = dropOffVal
        }

    } else {
        var dropOffAirpot = $('#drop_off_airport').val();
        var dropOffFligt = $('#drop_off_flight').val();
        var dropOffComing = $('#drop_off_coming').val();

        if (IsTextEmpty(dropOffAirpot)) {
            alert('Please Select Drop off Airport')
            return false;
        }
        if (IsTextEmpty(dropOffFligt)) {
            alert('Please Enter flight Number in Drop off Section')
            return false;
        }

        if (IsTextEmpty(dropOffComing)) {
            alert('Please Enter Coming from in Drop off Section')
            return false;
        }

        bodyObj.drop_off_airport = dropOffAirpot;
        bodyObj.drop_off_flight_number = dropOffFligt;
        bodyObj.drop_off_coming_from = dropOffComing;
        if ($('#airport_park_charge_drop_off_yes').is(':checked'))
            bodyObj.drop_off_airpot_parking = 'yes';
        if ($('#airport_park_charge_drop_off_no').is(':checked'))
            bodyObj.drop_off_airpot_parking = 'no';
    }
}

function extraSeatsSectionValue(bodyObj) {
    bodyObj.first_child_seat = $("#first_child").val();
    bodyObj.second_child_seat = $("#second_child").val();
    bodyObj.special_requirement = $("#special_require").val();
}


function passengerDetailsValidation(bodyObj) {
    var passenger_name = $("#passg_name").val();
    var passenger_last = $("#passg_last").val();
    var passenger_email = $("#passg_email").val();
    var passenger_mobile = $("#passg_mobile").val();
    if (IsTextEmpty(passenger_name)) {
        alert('Please Enter passenger Name');
        return false;
    }
    if (IsTextEmpty(passenger_last)) {
        alert('Please Enter passenger last name');
        return false;
    }

    if (IsTextEmpty(passenger_email)) {
        alert('Please Enter Passenger Email address')
        return false;
    }
    if (IsTextEmpty(passenger_mobile)) {
        alert('Please Enter Passenger Mobile number')
        return false;
    }
    bodyObj.passenger_name = passenger_name;
    bodyObj.passenger_last_name = passenger_last;
    bodyObj.passenger_email = passenger_email;
    bodyObj.passenger_mobile_code = $("#passg_code").val();
    bodyObj.passenger_mobile = passenger_mobile;
    bodyObj.passenger_phone_code = $("#passg_phone_code").val();
    bodyObj.passenger_phone = $("#passg_phone").val();
    bodyObj.luggage = $("#luggage").val();
    bodyObj.hand_luggage = $("#handluggage").val();
    bodyObj.number_of_passengers = $('#passenger').val();

}

function bookerDetailsValidation(bodyObj) {

    if ($('#is_passenger').is(':checked')) {
        return 'no validation required'
    }

    var booker_name = $("#booker_name").val();
    var booker_email = $("#booker_email").val();
    var booker_mobile = $("#booker_mobile").val();
    if (IsTextEmpty(booker_name)) {
        alert('Please Enter Booker\'s Name');
        return false;
    }
    if (IsTextEmpty(booker_email)) {
        alert('Please Enter Booker\'s Email address');
        return false;
    }

    if (IsTextEmpty(booker_mobile)) {
        alert('Please Enter Booker\'s Mobile number')
        return false;
    }

    bodyObj.booker_name = booker_name;
    bodyObj.booker_email = booker_email;
    bodyObj.booker_code = $("#booker_code").val();
    bodyObj.booker_mobile = booker_mobile;
    bodyObj.booker_home_code = $("#booker_home_code").val();
    bodyObj.booker_home_phone = $("#booker_home_phone").val();

}

function getFormDdata() {
    var bodyObj = {};
    var pickValidation = pickUpValidation(bodyObj);
    if (pickValidation === false) {
        return false
    }

    var dropValidation = dropUpValidation(bodyObj);
    if (dropValidation === false) {
        return false
    }
    var bookerValidation = bookerDetailsValidation(bodyObj);
    if (bookerValidation === false) {
        return false
    }

    var passengerValidation = passengerDetailsValidation(bodyObj);
    if (passengerValidation === false) {
        return false
    }

    extraSeatsSectionValue(bodyObj);

    return bodyObj;
}

function sendEmail() {
    var spinner = $('#overlay');
    var data = getFormDdata();
    if (data) {
        spinner.show();
        Object.keys(data).forEach(key => {
            if(data[key] === undefined || data[key] === '')
            {
                delete data[key];
            }
        });

//        console.log(data);

        var subject = "Flight Booking Details";

         $.ajax({
             url: 'sendEmail.php',
             method: 'POST',
             dataType: 'json',
             data: {
                 name: 'booker_name' in data ? data.booker_name : data.passenger_name,
                 email: 'booker_email ' in data ? data.booker_email : data.passenger_email,
                 subject: subject,
                 body: data,
             }, success: function (response) {
                 spinner.hide();
                 if (response.status === 'success') {
                     window.location.href = `/thanks.html`;
                 } else {
                     var decoded = $("<div/>").html(response.response).text();
                     alert(decoded);
                 }
             }
         })
    }
}

function IsTextEmpty(val) {
    var text = $.trim(val);
    if (text == "")
        return true;
    else if (text.length <= 0)
        return true;
    else
        return false;
}