<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="booking.css">
    <title>Title</title>
    <style type="text/css">
        #overlay {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            background: rgba(0,0,0,0.75) url(assets/loader.gif) no-repeat center center;
            z-index: 10000;
        }

    </style>
</head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<body>



<div id="fh5co-services-section">
    <div class="container">
        <div class="row">
            <span class="clsIsValidate none"></span>
            <div class="col-md-12">


                <div id="contentBook">
                    <div id="book-h1">
                        KURV LONDON BOOKING FORM
                    </div>

                    <div id="book">
                        <div id="book-left">
                            <div class="book-h3">
                                One Way Information
                            </div>

                            <!-- Section One Start -->

                            <div class="book-cver">
                                <div class="book-inner-h1">
                                    Pickup From
                                    <input type="radio" name="pickUpCheck" id="pickUpAddressCheck"   onclick="show1();"/>
                                    Address
                                    <input type="radio" name="pickUpCheck" id="pickUpAirport" onclick="show2();" checked/>
                                    Airport
                                </div>
                                <script>
                                    function show1() {
                                        document.getElementById('div1').style.display = 'none';
                                        document.getElementById('pickUpDiv').style.display = 'block';
                                    }

                                    function show2() {
                                        document.getElementById('div1').style.display = 'block';
                                        document.getElementById('pickUpDiv').style.display = 'none';
                                    }
                                </script>

                                <section class="hide" id="div1">
                                    <div class="book-list">
                                        <div class="book-list2">
                                            From
                                        </div>
                                        <div class="book-list3">


                                            <div class="book-list">
                                                <div class="book-list31">
                                                    <span class="red"></span><span id="ContentPlaceHolder1_Label10">Select Airport:</span>
                                                </div>
                                                <div class="">
                                                    <select id="pick_up_airport" name="" class="b-field">

                                                        <option selected="selected" value="">--Select Airport--
                                                        </option>
                                                        <option value="15582">BIRMINGHAM AIRPORT B26 3QJ</option>
                                                        <option value="13">GATWICK AIRPORT NORTH RH6 0PJ</option>
                                                        <option value="12">GATWICK AIRPORT SOUTH RH6 0PJ</option>
                                                        <option value="11">HEATHROW TERMINAL 1 TW6 1JS</option>
                                                        <option value="10">HEATHROW TERMINAL 2 TW6 1JS</option>
                                                        <option value="15">HEATHROW TERMINAL 4 TW6 3AA</option>
                                                        <option value="16">HEATHROW TERMINAL 3 TW6 1JS</option>
                                                        <option value="8">LUTON AIRPORT LU2 9LY</option>
                                                        <option value="7">HEATHROW TERMINAL 5 TW6 2GA</option>
                                                        <option value="9">LONDON CITY AIRPORT E16 2PX</option>
                                                        <option value="4628">LIVERPOOL AIRPORT L24 1YD</option>
                                                        <option value="1259">ROBIN HOOD AIRPORT DONCASTER DN9 3RH
                                                        </option>
                                                        <option value="17">SOUTHEND AIRPORT SS2 6YF</option>
                                                        <option value="634">BOURNEMOUTH AIRPORT BH23 6SE</option>
                                                        <option value="1258">EAST MIDLANDS AIRPORT DE74 2SA</option>
                                                        <option value="18">SOUTHAMPTON AIRPORT SO18 2NL</option>
                                                        <option value="4629">MANCHESTER AIRPORT M90 1QX</option>
                                                        <option value="2425">NEWCASTLE AIRPORT NE13 8BZ</option>
                                                        <option value="1260">HUMBERSIDE AIRPORT DN39 6YH</option>
                                                        <option value="4">BRISTOL AIRPORT BS48 3DY</option>
                                                        <option value="1261">LEEDS BRADFORD AIRPORT LS19 7TU</option>
                                                        <option value="6">STANSTED AIRPORT CM24 1QW</option>
                                                        <option value="2451">DUNDEE AIRPORT DD2 1UH</option>
                                                        <option value="2449">GLASGOW AIRPORT PA3 2SW</option>
                                                        <option value="5">HEATHROW AIRPORT TW6 2GA</option>
                                                        <option value="2452">INVERNESS AIRPORT IV2 7JB</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="book-list" id="div1" class="hide">
                                        <div class="book-list2">
                                            <span class="" style="color: red;">*</span>
                                            <span id="">Flight Number</span>
                                        </div>
                                        <div class="book-list3">
                                            <input id="pick_up_flight"
                                                   name="ctl00$ContentPlaceHolder1$rptFromtype$ctl00$txtRsult"
                                                   type="text" class="b-field clsRequired">
                                        </div>
                                    </div>

                                    <div class="book-list">
                                        <div class="book-list2">
                                            <span class="" style="color: red;">*</span>
                                            <span id="">Coming From</span>
                                        </div>
                                        <div class="book-list3">
                                            <input id="pick_up_coming"
                                                   name="ctl00$ContentPlaceHolder1$rptFromtype$ctl01$txtRsult"
                                                   type="text" class="b-field clsRequired">
                                        </div>
                                    </div>

                                    <div id="ctl00_ContentPlaceHolder1_pnlAirportPickupCharges">

                                        <div class="book-list">
                                            <div class="book-inner-h1">
                                                For inside airport pickup, car park charges are exclusive in this price.
                                                Customer will be charge end of the journey.
                                            </div>
                                            <div class="book-list3">
                                                <span class="clsmeetoutside"><input value="no" type="radio"
                                                                                    name="ctl00$ContentPlaceHolder1$AirportCharge"
                                                                                    id="airport_park_charge_pick_no" checked /><label
                                                            for="ctl00_ContentPlaceHolder1_rdoAirportchargeNo">N0, outside</label></span>
                                                <span class="clsmeetgreet"><input value="yes" type="radio"
                                                                                  name="ctl00$ContentPlaceHolder1$AirportCharge"
                                                                                  id="airport_park_charge_pick_yes"><label
                                                            for="ctl00_ContentPlaceHolder1_rdoAirportchargeYes">YES, Inside</label></span>


                                            </div>
                                        </div>


                                    </div>

                            </div>

                            </section>


                            <div class="book-cver" id="pickUpDiv" style="display: none">
                                <div class="book-inner-h1">
                                    <div><textarea id="pick_up_address" name="bournamet_port" rows="2" cols="20"
                                                   value="bournamet_port" class="b-field"></textarea>
                                    </div>
                                </div>

                            </div>

                            <!-- Section one Ends -->


                            <!-- Section Two Start -->

                            <div class="book-cver">
                                <div class="book-inner-h1">
                                    Drop Off
                                    <input type="radio" id="dropUpAddressCheck" name="tab" value="adress" onclick="show11();"  checked/>
                                    Address
                                    <input type="radio" id="tab" name="tab" value="airport" onclick="show22();"/>
                                    Airport
                                </div>
                                <script>
                                    function show11() {
                                        document.getElementById('dropAirportDiv').style.display = 'none';
                                        document.getElementById('dropAddressDiv').style.display = 'block';
                                    }

                                    function show22() {
                                        document.getElementById('dropAddressDiv').style.display = 'none';
                                        document.getElementById('dropAirportDiv').style.display = 'block';
                                    }
                                </script>

                                <section class="hide" style="display: none" id="dropAirportDiv">
                                    <div class="book-list">
                                        <div class="book-list2">
                                            From
                                        </div>
                                        <div class="book-list3">


                                            <div class="book-list">
                                                <div class="book-list31">
                                                    <span class="red"></span><span id="ContentPlaceHolder1_Label10">Select Airport:</span>
                                                </div>
                                                <div class="">
                                                    <select id="drop_off_airport" name="" class="b-field">

                                                        <option selected="selected" value="">--Select Airport--
                                                        </option>
                                                        <option value="15582">BIRMINGHAM AIRPORT B26 3QJ</option>
                                                        <option value="13">GATWICK AIRPORT NORTH RH6 0PJ</option>
                                                        <option value="12">GATWICK AIRPORT SOUTH RH6 0PJ</option>
                                                        <option value="11">HEATHROW TERMINAL 1 TW6 1JS</option>
                                                        <option value="10">HEATHROW TERMINAL 2 TW6 1JS</option>
                                                        <option value="15">HEATHROW TERMINAL 4 TW6 3AA</option>
                                                        <option value="16">HEATHROW TERMINAL 3 TW6 1JS</option>
                                                        <option value="8">LUTON AIRPORT LU2 9LY</option>
                                                        <option value="7">HEATHROW TERMINAL 5 TW6 2GA</option>
                                                        <option value="9">LONDON CITY AIRPORT E16 2PX</option>
                                                        <option value="4628">LIVERPOOL AIRPORT L24 1YD</option>
                                                        <option value="1259">ROBIN HOOD AIRPORT DONCASTER DN9 3RH
                                                        </option>
                                                        <option value="17">SOUTHEND AIRPORT SS2 6YF</option>
                                                        <option value="634">BOURNEMOUTH AIRPORT BH23 6SE</option>
                                                        <option value="1258">EAST MIDLANDS AIRPORT DE74 2SA</option>
                                                        <option value="18">SOUTHAMPTON AIRPORT SO18 2NL</option>
                                                        <option value="4629">MANCHESTER AIRPORT M90 1QX</option>
                                                        <option value="2425">NEWCASTLE AIRPORT NE13 8BZ</option>
                                                        <option value="1260">HUMBERSIDE AIRPORT DN39 6YH</option>
                                                        <option value="4">BRISTOL AIRPORT BS48 3DY</option>
                                                        <option value="1261">LEEDS BRADFORD AIRPORT LS19 7TU</option>
                                                        <option value="6">STANSTED AIRPORT CM24 1QW</option>
                                                        <option value="2451">DUNDEE AIRPORT DD2 1UH</option>
                                                        <option value="2449">GLASGOW AIRPORT PA3 2SW</option>
                                                        <option value="5">HEATHROW AIRPORT TW6 2GA</option>
                                                        <option value="2452">INVERNESS AIRPORT IV2 7JB</option>

                                                    </select>
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                    <div class="book-list" id="div1" class="hide">
                                        <div class="book-list2">
                                            <span class="" style="color: red;">*</span>
                                            <span id="ctl00_ContentPlaceHolder1_rptFromtype_ctl00_lblLabel">Flight Number</span>
                                        </div>
                                        <div class="book-list3">
                                            <input id="drop_off_flight"
                                                   name="ctl00$ContentPlaceHolder1$rptFromtype$ctl00$txtRsult"
                                                   type="text" class="b-field clsRequired">
                                        </div>
                                    </div>

                                    <div class="book-list">
                                        <div class="book-list2">
                                            <span class="" style="color: red;">*</span>
                                            <span id="ctl00_ContentPlaceHolder1_rptFromtype_ctl01_lblLabel">Coming From</span>
                                        </div>
                                        <div class="book-list3">
                                            <input id="drop_off_coming"
                                                   name="ctl00$ContentPlaceHolder1$rptFromtype$ctl01$txtRsult"
                                                   type="text" class="b-field clsRequired">
                                        </div>
                                    </div>

                                    <div id="ctl00_ContentPlaceHolder1_pnlAirportPickupCharges">

                                        <div class="book-list">
                                            <div class="book-inner-h1">
                                                For inside airport pickup, car park charges are exclusive in this price.
                                                Customer will be charge end of the journey.
                                            </div>
                                            <div class="book-list3">
                                                <span class="clsmeetoutside"><input value="no" type="radio"
                                                                                    name="ctl00$ContentPlaceHolder1$AirportCharge"
                                                                                    id="airport_park_charge_drop_off_no" checked><label
                                                            for="ctl00_ContentPlaceHolder1_rdoAirportchargeNo">N0, outside</label></span>
                                                <span class="clsmeetgreet"><input value="yes" type="radio"
                                                                                  name="ctl00$ContentPlaceHolder1$AirportCharge"
                                                                                  id="airport_park_charge_drop_off_yes"><label
                                                            for="ctl00_ContentPlaceHolder1_rdoAirportchargeYes">YES, Inside</label></span>


                                            </div>
                                        </div>


                                    </div>

                            </div>



                            <div class="book-cver" id="dropAddressDiv" >
                                <div class="book-inner-h1">
                                    <div><textarea id="drop_up_address" name="bournamet_port" rows="2" cols="20"
                                                   class="b-field"></textarea>
                                    </div>
                                </div>

                            </div>

                            <!-- Section Two Ends -->


                            <div class="book-inner-h1">
                                Extra Seat
                            </div>
                            <div class="book-list">
                                <div class="book-list2">
                                    First Child Seat(Free):
                                </div>
                                <div class="book-list3">
                                    <select name="ctl00$ContentPlaceHolder1$ddlFirstChildSeat" id="first_child"
                                            class="b-field3">
                                        <option value="No child seat required">No child seat required</option>
                                        <option value="Rear-facing infant seat (suitable for babies)">Rear-facing infant
                                            seat (suitable for babies)
                                        </option>
                                        <option value="Forward-facing upring child seat (for toddlers and smaller children)">
                                            Forward-facing upring child seat (for toddlers and smaller children)
                                        </option>
                                        <option value="Child booster seat">Child booster seat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="book-list">
                                <div class="book-list2">
                                    Second Child Seat(Free):
                                </div>
                                <div class="book-list3">
                                    <select name="ctl00$ContentPlaceHolder1$ddlSecondChild" id="second_child"
                                            class="b-field3">
                                        <option value="No child seat required">No child seat required</option>
                                        <option value="Rear-facing infant seat (suitable for babies)">Rear-facing infant
                                            seat (suitable for babies)
                                        </option>
                                        <option value="Forward-facing upring child seat (for toddlers and smaller children)">
                                            Forward-facing upring child seat (for toddlers and smaller children)
                                        </option>
                                        <option value="Child booster seat">Child booster seat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="book-list">
                                <div class="book-list2">
                                    Special Requirement:
                                </div>
                                <div class="book-list3">
                                    <textarea rows="2" name="special_requirements" cols="20" id="special_require"
                                              class="b-field" autocomplete="off"></textarea>
                                </div>
                            </div>


                        </div>


                        <div id="book-right">


                            <div class="book-h3">
                                Passenger Details
                            </div>

                            <div class="book-cver">

                                <div id="ContentPlaceHolder1_Update3">
                                    <div id="ContentPlaceHolder1_pnlcheckpassenger">
                                        <div class="book-list none">
                                            <div class="book-list2">
                                                Are you the Passenger?
                                            </div>
                                            <div class="book-list2">


                                                <input type="radio" id="is_passenger" name="tab1" value="yes"
                                                       onclick="show101();"/>
                                                Yes
                                                <input type="radio" id="tab1" name="tab1" value="no" checked
                                                       onclick="show202();"/>
                                                No
                                            </div>
                                        </div>


                                        <script>
                                            function show101() {
                                                document.getElementById('div4').style.display = 'none';
                                            }

                                            function show202() {
                                                document.getElementById('div4').style.display = 'block';
                                            }
                                        </script>


                                        <div class="book-list clsBooker none" id="div4">
                                            <div class="book-inner-h1">
                                                Booker's Details:
                                            </div>
                                            <div class="book-list">
                                                <div class="book-list2">
                                                    <span class="red">*</span><span>Booker's Full Name:</span>
                                                </div>
                                                <div class="book-list3">
                                                    <input name="ctl00$ContentPlaceHolder1$txtBookerFullName"
                                                           type="text" id="booker_name" class="b-field clsRequired"
                                                           autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="book-list">
                                                <div class="book-list2">
                                                    <span class="red">*</span><span>Booker's Email:</span>
                                                </div>
                                                <div class="book-list3">
                                                    <input name="ctl00$ContentPlaceHolder1$txtbookeremail" type="text"
                                                           id="booker_email" class="b-field clsRequired"
                                                           autocomplete="off" onpaste="return false">
                                                    <span id="ctl00_ContentPlaceHolder1_rev"
                                                          style="color:Red;visibility:hidden;">Valid Email Required</span>
                                                </div>
                                            </div>
                                            <!--<div class="book-list">
                                                <div class="book-list2">
                                                    <span class="red">*</span><span>Re-confirm booker's email:</span>
                                                </div>
                                                <div class="book-list3">
                                                    <input name="ctl00$ContentPlaceHolder1$txtReconfirmBookeremail"
                                                           type="text" id="re_confrm_ email" class="b-field clsRequired"
                                                           autocomplete="off" onpaste="return false">
                                                    <span id="ctl00_ContentPlaceHolder1_revRconfirm"
                                                          style="color:Red;visibility:hidden;">Valid Email Required</span>
                                                    <br>
                                                    <span id="ctl00_ContentPlaceHolder1_cvEmail"
                                                          style="color:Red;visibility:hidden;">Email and Confirm Email must be same.</span>
                                                </div>
                                            </div>-->
                                            <div class="book-list">
                                                <div class="book-list2">
                                                    <span class="red">*</span><span id="ContentPlaceHolder1_Label10">Bookers Mobile No:</span>
                                                </div>
                                                <div class="book-list3">
                                                    <select name="ctl00$ContentPlaceHolder1$ddlbookerMobileNo"
                                                            id="booker_code" class="b-field">
                                                        <option selected="selected" value="+44">United Kingdom-44
                                                        </option>
                                                        <option value="+380">Ukraine-380</option>
                                                        <option value="+598">Uruguay-598</option>
                                                        <option value="+1">United States-1</option>
                                                        <option value="+998">Uzbekistan-998</option>
                                                        <option value="+678">Vanuatu-678</option>
                                                        <option value="+39">Holy See (vatican City State)-39</option>
                                                        <option value="+58">enezuela, Bolivarian Republic Of-58</option>

                                                    </select>
                                                    <input name="ctl00$ContentPlaceHolder1$txtBookerMobileNo" type="tel"
                                                           id="booker_mobile" class="b-field clsRequired">
                                                </div>
                                            </div>
                                            <div class="book-list">
                                                <div class="book-list2">
                                                    <span id="ContentPlaceHolder1_Label11">Bookers Home No:</span>
                                                </div>
                                                <div class="book-list3">
                                                    <select name="ctl00$ContentPlaceHolder1$ddlbookerHomeNo"
                                                            id="booker_home_code" class="b-field">
                                                        <option selected="selected" value="+44">United Kingdom-44
                                                        </option>
                                                        <option value="+380">Ukraine-380</option>
                                                        <option value="+598">Uruguay-598</option>
                                                        <option value="+1">United States-1</option>
                                                        <option value="+998">Uzbekistan-998</option>
                                                        <option value="+678">Vanuatu-678</option>
                                                        <option value="+39">Holy See (vatican City State)-39</option>
                                                        <option value="+58">enezuela, Bolivarian Republic Of-58</option>

                                                    </select>
                                                    <input name="ctl00$ContentPlaceHolder1$txtBookerHomeNo" type="text"
                                                           id="booker_home_phone" class="b-field clsRequired">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="book-list clsPassanger">
                                        <div class="book-inner-h1">
                                            Passenger's Details:
                                        </div>
                                        <div class="book-list">
                                            <div class="book-list2">
                                                <span class="red">*</span>Name:
                                            </div>
                                            <div class="book-list3">

                                                <input name="ctl00$ContentPlaceHolder1$txtPassangerFirstName"
                                                       type="text" id="passg_name" class="b-field clsRequired"
                                                       autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="book-list none">
                                            <div class="book-list2">
                                                <span class="red">*</span>Last Name:
                                            </div>
                                            <div class="book-list3">

                                                <input name="ctl00$ContentPlaceHolder1$txtPassangerLastName" type="text"
                                                       id="passg_last" class="b-field clsRequired" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="book-list">
                                            <div class="book-list2">
                                                <span class="red">*</span>Email Address:
                                            </div>
                                            <div class="book-list3">

                                                <input name="ctl00$ContentPlaceHolder1$txtPassangerEmail" type="text"
                                                       id="passg_email" class="b-field clsRequired" autocomplete="off"
                                                       onpaste="return false">
                                                <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator2"
                                                      style="color:Red;visibility:hidden;">Valid Email Required</span>
                                            </div>
                                        </div>
                                        <!--<div class="book-list">
                                            <div class="book-list2">
                                                <span class="red">*</span>Re-confirm Email:
                                            </div>
                                            <div class="book-list3">

                                                <input name="ctl00$ContentPlaceHolder1$txtPassangerReConfEmail"
                                                       type="text" id="passg_reemail" class="b-field clsRequired"
                                                       autocomplete="off" onpaste="return false">
                                                <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator1"
                                                      style="color:Red;visibility:hidden;">Valid Email Required</span>
                                                <br>
                                                <span id="ctl00_ContentPlaceHolder1_CompareValidator1"
                                                      style="color:Red;visibility:hidden;">Email and Confirm Email must be same.</span>
                                            </div>
                                        </div>-->
                                        <div class="book-list">
                                            <div class="book-list2">
                                                <span class="red">*</span>Mobile Number:
                                            </div>
                                            <div class="book-list3">
                                                <select name="ctl00$ContentPlaceHolder1$ddlMobileNumber" id="passg_code"
                                                        class="b-field3">
                                                    <option selected="selected" value="+44">United Kingdom-44</option>
                                                    <option value="+380">Ukraine-380</option>
                                                    <option value="+598">Uruguay-598</option>
                                                    <option value="+1">United States-1</option>
                                                    <option value="+998">Uzbekistan-998</option>
                                                    <option value="+678">Vanuatu-678</option>
                                                    <option value="+39">Holy See (vatican City State)-39</option>
                                                    <option value="+58">enezuela, Bolivarian Republic Of-58</option>

                                                </select>

                                                <input name="ctl00$ContentPlaceHolder1$txtPassangerMobileNumber"
                                                       type="tel" id="passg_mobile" class="b-field clsRequired"
                                                       autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="book-list">
                                            <div class="book-list2">
                                                <span class="red"></span>Phone Number:
                                            </div>
                                            <div class="book-list3">
                                                <select name="ctl00$ContentPlaceHolder1$ddlHomeNumber"
                                                        id="passg_phone_code" class="b-field3">
                                                    <option selected="selected" value="+44">United Kingdom-44</option>
                                                    <option value="+380">Ukraine-380</option>
                                                    <option value="+598">Uruguay-598</option>
                                                    <option value="+1">United States-1</option>
                                                    <option value="+998">Uzbekistan-998</option>
                                                    <option value="+678">Vanuatu-678</option>
                                                    <option value="+39">Holy See (vatican City State)-39</option>
                                                    <option value="+58">enezuela, Bolivarian Republic Of-58</option>
                                                </select>
                                                <input name="ctl00$ContentPlaceHolder1$txtpassangerHomeNumber"
                                                       type="text" id="passg_phone" class="b-field" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="book-list">
                                        <div class="book-list4">
                                            &nbsp;Passengers:&nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$ddlPassangers" id="passenger"
                                                    class="b-field2 clsRequired">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                            &nbsp;Luggages:&nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$ddlLuggages" id="luggage"
                                                    class="b-field2">
                                                <option value="N/A">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                            &nbsp;Hand Lugg:&nbsp;
                                            <select name="ctl00$ContentPlaceHolder1$ddlHandLugg" id="handluggage"
                                                    class="b-field2">
                                                <option value="N/A">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="btn1" >
                            <input type="button" class="btn1" onclick="sendEmail()" value="Submit">
                            <center><p>Powered By <a href="http://www.mmwidevisions.com/">MM Wide Visions</a></p>
                            </center>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="overlay">
</div>
<script src="script.js" type="text/javascript"></script>
</body>

</html>