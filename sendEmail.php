<?php
    use PHPMailer\PHPMailer\PHPMailer;

    if (isset($_POST['name']) && isset($_POST['email'])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $body = $_POST['body'];
        
        require_once "PHPMailer.php";
        require_once "SMTP.php";
        require_once "Exception.php";
        require_once "html2pdf.php";

        $mail = new PHPMailer();

        $emailHTML = '<div><h1><p><strong>KURV LONDON BOOKING FORM</strong></p><p></h1><p>Booked By: <i>'. $email .'</i></p>';
        foreach ($body as $key => $value){
            $emailHTML .= '<p><b>' . ucfirst(str_replace('_', ' ', $key)) . '</b>: ' . $value . '</p>';
        }
        $emailHTML .= '</div>';
        //SMTP Settings
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;

        $mail->Username = "Kurvjobs@gmail.com";
        $mail->Password = 'Kurvjobs@1';

        $mail->Port = 465; //587
        $mail->SMTPSecure = "ssl"; //tls

        //Email Settings
        $mail->isHTML(true);
        $mail->setFrom($email, $name);
        $mail->AddBCC("masadashraf@ymail.com", "Admin");
        $mail->addAddress($email);
        $mail->Subject = $subject;
        $mail->Body = $emailHTML;

        $pdf=new PDF_HTML();
        $pdf->SetFont('Arial','',12);
        $pdf->AddPage();
        $pdf->WriteHTML($emailHTML);
        $filename = __DIR__  .'/receipt_'. date('Ymdhis') .'.pdf';
        $file = $pdf->Output('F', $filename);
        $mail->AddAttachment($filename, '', $encoding = 'base64', $type = 'application/pdf');
        if ($mail->send()) {
            $status = "success";
            $response = "Email is sent!";
        } else {
            $status = "failed";
            $response = "Something is wrong: <br><br>" . $mail->ErrorInfo;
        }
        unlink($filename);

        exit(json_encode(array("status" => $status, "response" => $response)));
    }else{
        $status = "failed";
        $response = "Email and Name is required ";
        exit(json_encode(array("status" => $status, "response" => $response)));
    }
?>
